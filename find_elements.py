from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

path = "D:\Descargas\Programs\chromedriver.exe"
driver = webdriver.Chrome(path)

# driver.get("http://scanme.nmap.org/")
driver.get("https://techwithtim.net/")
search = driver.find_element_by_name("s")

search.send_keys("scan")
search.send_keys(Keys.RETURN)

try:
    main = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, "main"))
    )
    # print(web_result.text)
    articles = main.find_elements_by_tag_name("article")
    for article in articles:
        header = article.find_element_by_class_name("entry-summary")
        print(header.text)

finally:
    driver.quit()
# web_result = driver.find_element_by_class_name("gsc-expansionArea")
# time.sleep(2)
# print(web_result.text)
